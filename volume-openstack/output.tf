output "id" {
  value = openstack_blockstorage_volume_v3.volume.id
}

output "volume_id" {
  value = openstack_blockstorage_volume_v3.volume.id
}

output "volume_name" {
  value = openstack_blockstorage_volume_v3.volume.name
}

output "volume_size" {
  value = var.volume_size
}

output "volume_type" {
  value = var.volume_type
}
