resource "openstack_blockstorage_volume_v3" "volume" {
  name        = var.workspace_id
  size        = var.volume_size
  volume_type = var.volume_type
  metadata = {
    workspace_id       = var.workspace_id
    subscription       = var.subscription
    application_type   = var.application_type
    resource_type      = var.resource_type
    cloud_type         = var.cloud_type
    subscription_group = var.subscription_group
  }
}
