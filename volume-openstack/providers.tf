provider "openstack" {
  auth_url                      = var.auth_url
  tenant_id                     = var.project_id
  tenant_name                   = var.project_name
  application_credential_id     = var.user_name
  application_credential_secret = var.password
  region                        = var.region
}
